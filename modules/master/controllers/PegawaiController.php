<?php

namespace app\modules\master\controllers;

use app\modules\master\models\Agama;
use app\modules\master\models\City;
use app\modules\master\models\Divisi;
use app\modules\master\models\Jabatan;
use Yii;
use app\modules\master\models\Pegawai;
use app\modules\master\models\PegawaiSearch;
use app\modules\master\models\Province;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PegawaiController implements the CRUD actions for Pegawai model.
 */
class PegawaiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pegawai models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pegawai model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function getAgama() {

        $model = Agama::find()->asArray()->all();
        for ($i = 0; $i < count($model); $i++) {
            $id[$i] = $model[$i]['id'];
            $agama[$id[$i]] = $model[$i]['nama'];

        }
        
        return $agama; 
    }

    public function actionCity() {

        if (Yii::$app->request->isAjax) {
            $model = City::find()->where(['province_id' => Yii::$app->request->get()['id']])->asArray()->all();
            /*for ($i = 0; $i < count($model); $i++) {
                $id[$i] = $model[$i]['id'];
                $city[$id[$i]] = $model[$i]['name'];

            }*/
            $city = json_encode($model);

            return $city; 
        }
        
    }

    public function getDivisi() {

        $model = Divisi::find()->asArray()->all();
        for ($i = 0; $i < count($model); $i++) {
            $id[$i] = $model[$i]['id'];
            $divisi[$id[$i]] = $model[$i]['nama'];

        }
        
        return $divisi; 
    }

    public function getJabatan() {

        $model = Jabatan::find()->asArray()->all();
        for ($i = 0; $i < count($model); $i++) {
            $id[$i] = $model[$i]['id'];
            $jabatan[$id[$i]] = $model[$i]['nama'];

        }
        
        return $jabatan; 
    }

    public function getProvince() {

        $model = Province::find()->asArray()->all();
        $province['nan'] = 'Pilih Province ...';
        for ($i = 0; $i < count($model); $i++) {
            $id[$i] = $model[$i]['id'];
            $province[$id[$i]] = $model[$i]['name'];

        }
        
        return $province; 
    }

    /**
     * Creates a new Pegawai model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pegawai();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'agama' => $this->getAgama(),
            'city' => ['nan' => 'Pilih Provinsi Terlebhi dahulu'],
            'divisi' => $this->getDivisi(),
            'jabatan' => $this->getJabatan(),
            'province' => $this->getProvince(),
        ]);
    }

    /**
     * Updates an existing Pegawai model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pegawai model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pegawai the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pegawai::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
