# UTS PWL

Project template UTS Pemrograman Web Lanjutan.

## Aturan Ujian

- Maksimal waktu pengerjaan ujian **10 jam**, dihitung dari waktu mulai ujian yang sesuai jadwal.

- Jawaban di-upload pada project gitlab kalian masing-masing. Invite saya **@edoriansyah** sebagai *developer* pada project kalian.

- Link project dikumpulkan pada **assignment** yang sudah disediakan di elen.

- Dilarang keras melakukan copy paste pekerjaan teman.

- Sebelum memulai ujian wajib membaca tahapan memulai dibawah ini terlebih dahulu.

## Tahapan Memulai

### 1. Download project

Klik link [download project](https://gitlab.com/web-lanjut-sttnf-20192/uts/-/archive/master/uts-master.zip) ini terlebih dahulu, setelah itu extract project ke folder **htdocs** (xampp) atau **html** (apache2 linux).

Ubah nama folder project menjadi **uts-pwl**.

### 2. Composer update

Lakukan composer update pada folder **uts-pwl**, dengan masuk ke folder uts-pwl terlebih dahulu melalui *Command Line*.

Untuk Windows

```
cd C:\xampp\htdocs\uts-pwl
```

Untuk Linux

```
cd /var/www/html/uts-pwl
```

Setelah itu jalankan

```
composer update
```

Jika belum meng-install *composer*, silakan di-install terlebih dahulu. Kunjungi [link ini](https://getcomposer.org/doc/00-intro.md#installation-windows).

### 3. Buat project baru

Pada akun gitlab kalian masing-masing buatlah project baru *(private)* dengan nama **uts-pwl**.

Project baru ini digunakan untuk mengumpulkan jawaban UTS kalian.

Jangan lupa invite saya **@edoriansyah** sebagai *developer* pada project kalian.

### 4. Buat database & import

- Buatlah database **dbkepegawaian** pada phpMyAdmin.
- Lakukan import file [**dbkepegawaian.sql**](https://gitlab.com/web-lanjut-sttnf-20192/uts/-/blob/master/dbkepegawaian.sql) yang ada pada template project ini ke database dbkepegawaian. Gunakan phpMyAdmin!

Tahapan selesai, silakan kerjakan soalnya.

## Referensi link extension

- [Yii2 Widgets](http://demosbs3.krajee.com/widgets)
