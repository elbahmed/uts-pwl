
$(document).ready(function() {
    $('#id_province').children().click(function(e) {
        e.preventDefault;
        if($(this).val() == "nan") {
            $('#id_city').children().detach();
            $('#id_city').prop('disabled', true);
            $('#id_city').append('<option value="nan"> -- pilih kota -- </option>');
        } else {
            $('#id_city').children().detach();
            $('#id_city').prop('disabled', false);
            var data = { 'id': $(this).val() };
            $.ajax({
                data: data,
                url: 'city',
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    $('#id_city').append('<option value="nan"> -- pilih kota -- </option>');
                    for (var i = 0; i < response.length; i++) {
                        $('#id_city').append("<option value=\""+response[i].id+"\">"+response[i].name+"</option>");
                    }
                },
                error: function(xhr, errMsg, err) {
                    $("#msg").text('error');
                }
            });
        }
    });
});